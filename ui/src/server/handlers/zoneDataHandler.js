const needle = require('needle')

async function getZoneData(req, res) {
  try {
    const url = `${process.env.ZONEMGR_URL}/view-zone`
    const zoneData = await needle('get',url, {
      headers: {
        authorization: `Bearer ${process.env.API_KEY}`
      },
      json: true
    })
    res.json(zoneData.body)  
  } catch (error) {
    res.status(500).send('Error: ' + error.message)
  }
}

module.exports = {
  getZoneData
}