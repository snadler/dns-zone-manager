const ViteExpress = require('vite-express')
const app = require('./router')
require('dotenv').config()


const port = process.env.NODE_ENV === 'production' ? 80 : 8080

ViteExpress.listen(app, port, () =>
  console.log(`Server is listening on port ${port}...`),
);
