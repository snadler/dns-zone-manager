const supertest = require('supertest')
const server = require('../src/server/router')
const nock = require('nock')
const mockData = require('./mockData/mockData')
require('dotenv').config()

describe('GET /api/zonedata', () => {
  it('should return zoneData', async ()=> {
    const zoneMockData = require('./mockData/mockData')
    const mockScope = nock(process.env.ZONEMGR_URL)
      .get('/view-zone')
      .reply(200, mockData)
    const response = await supertest(server).get('/api/zonedata')
    expect(response.statusCode).toBe(200)
    expect(response.body).toEqual(mockData)
  })
})

describe('GET /api/zonedata', () => {
  it('should fail and return 500 error', async ()=> {
    const response = await supertest(server).get('/api/zonedata')
    expect(response.statusCode).toBe(500)
  })
})