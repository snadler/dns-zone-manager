#! /bin/bash
#
# Setup script. It will take the configuration file and will initialize an intermediate sql DB, 
# fill it with initial records for the creation of zone file. It also configures and sets the nsd
# server up and starts the Zone Manager server.
#
# Must be called as ./script.sh $PORT
# The Zone Manager server will start at $PORT number.

printf "====>>> STARTING ZONE MANAGER SERVICE SETUP \n"

printf "Setting up NSD (default configuration)... \n"
# Configure NSD server
cat > /tmp/server.conf <<EOF
server:
    ip-address: 0.0.0.0
    ip-address: ::0
    verbosity: 3
    pidfile: "/var/run/nsd/nsd.pid"
    #port: 5353
EOF
mv /tmp/server.conf /etc/nsd/nsd.conf.d/server.conf

cat > /tmp/remote.conf << EOF
remote-control:
    control-enable: yes
    control-interface: 0.0.0.0
    control-port: 8952
    server-key-file: "/etc/nsd/nsd_server.key"
    server-cert-file: "/etc/nsd/nsd_server.pem"
    control-key-file: "/etc/nsd/nsd_control.key"
    control-cert-file: "/etc/nsd/nsd_control.pem"
EOF
mv /tmp/remote.conf /etc/nsd/nsd.conf.d/remote.conf

service nsd start
printf "\n"
service nsd status

printf "\nGetting DNS configuration variables from config file... \n"
# Get configuration variables from the config file
CONFIG_FILE="config.conf"
if [ -f "$CONFIG_FILE" ]; then
    source "$CONFIG_FILE"
    printf "  Config file found. Trust framework domain name to set: %s\n" "$TF_DOMAIN_NAME"
else
    printf "  ERROR: Configuration file \"$CONFIG_FILE\" does not exist. \n"
    exit 1
fi

ZM_PATH="/usr/lib/zonemgr/"		# path for source code.
VAR_PATH="/var/lib/zonemgr/"	# path for variables including DB, zonefile and DNS config file
ZMGR=$ZM_PATH"zonemanager.py"
SQLITE_DB="sqlite:///"$VAR_PATH"zones.db"
export SQLALCHEMY_WARN_20=1

# set permissions, add user and copy the ZM service to system folder
chmod 777 -R "$ZM_PATH"
adduser --system --home "$VAR_PATH" zonemgr


printf "Initializing intermediate DB and adding records to it and to the zone file... \n"
printf "NSD server will be reload and reconfigured serveral times. This will print 'ok' and 'reconfig start' messages repeatedly: \n\n"
SQLALCHEMY_SILENCE_UBER_WARNING=1

# initialize DB
python3 "$ZM_PATH"zonemanager.py --database "$SQLITE_DB" init

# add default environment
python3 "$ZMGR" \
  --database $SQLITE_DB \
  add-environment \
	--environment network \
	--nsd-name "$PRIMARY_SERVER_NDS" \
	--nsd-conf "$VAR_PATH"nsd.zones.conf \
	--nsd-reload "$ZM_PATH"reload-nsd.sh \
	--key-file "$VAR_PATH"private_key.tmp

# add default zone with given trust framework domain
python3 "$ZMGR" \
  --database $SQLITE_DB \
  add-zone \
	--environment network \
	--apex "$TF_DOMAIN_NAME" \
	--pattern network

chown -R zonemgr: /var/lib/zonemgr

# add required NS records and related IP addresses
python3 "$ZMGR" \
  --database $SQLITE_DB \
  add-record \
    --environment network \
    --apex "$TF_DOMAIN_NAME" \
    "$TF_DOMAIN_NAME" NS \
      "$PRIMARY_SERVER_NDS"  \
      "$SECONDARY_SERVER_1_NDS"

python3 "$ZMGR" \
  --database $SQLITE_DB \
  add-record \
    --environment network \
    --apex "$TF_DOMAIN_NAME" \
      "$TF_DOMAIN_NAME" A "$TF_DOMAIN_IP"

python3 "$ZMGR" \
  --database $SQLITE_DB \
  add-record \
    --environment network \
    --apex "$TF_DOMAIN_NAME" \
      "$PRIMARY_SERVER_NDS" A "$PRIMARY_SERVER_IP"

python3 "$ZMGR" \
  --database $SQLITE_DB \
  add-record \
    --environment network \
    --apex "$TF_DOMAIN_NAME" \
      "$SECONDARY_SERVER_1_NDS" A "$SECONDARY_SERVER_1_IP"

python3 "$ZMGR" \
  --database $SQLITE_DB \
  add-record \
    --environment network \
    --apex "$TF_DOMAIN_NAME" \
      "$SECONDARY_SERVER_2_NDS" A "$SECONDARY_SERVER_2_IP"

printf "\nAdding cron job for resigning... \n"

# Install the DNSSEC resigning cronjob:
crontab -u zonemgr "$ZM_PATH"etc/crontab

printf "\nConfiguring NSD for ZM... \n"

cat > /tmp/zonemgr.conf <<EOF
pattern:
    name: network
    notify: $SECONDARY_SERVER_1_IP NOKEY
    #notify: $SECONDARY_SERVER_2_IP NOKEY
    provide-xfr: $SECONDARY_SERVER_1_IP NOKEY
    #provide-xfr: $SECONDARY_SERVER_2_IP NOKEY

include: /var/lib/zonemgr/nsd.zones.conf
EOF
mv /tmp/zonemgr.conf /etc/nsd/nsd.conf.d/zonemgr.conf

service nsd start
printf "\n"
service nsd status


# Generate and print a token (API KEY) that the TSPA must use to call ZM endpoints
printf "\nGenerating a ZM API KEY for communication with TSPA, please note it down: \n"
printf "=================================================================== \n"
python3 "$ZMGR" -d $SQLITE_DB create-token \
    --environment network\
    network \
    "$TF_DOMAIN_NAME"
printf "=================================================================== \n"

printf "\nStarting the Zone Manager service in port %s... \n\n" "$1"
$ZMGR --database $SQLITE_DB server -e network 0.0.0.0:$1
